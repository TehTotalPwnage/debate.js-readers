module.exports = function (BaseReader) {
  class LosAngelesTimesReader extends BaseReader {
    static get basename () {
      return 'www.latimes.com'
    }

    static get source () {
      return 'Los Angeles Times'
    }

    get author () {
      return this.$('meta[name=author]').attr('content')
    }

    get content () {
      let html = ''
      let that = this
      this.$('.pb-f-article-body .collection [data-type=text] div p').each(function () {
        html += that.$('<div>').append(that.$(this).clone()).html()
      })
      return html
    }

    get datetime () {
      return this.$('meta[name=date]').attr('content')
    }

    get title () {
      return this.$('h1').text()
    }
  }
  return LosAngelesTimesReader
}
