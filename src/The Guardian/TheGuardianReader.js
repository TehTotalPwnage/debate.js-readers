module.exports = function (BaseReader) {
  class TheGuardianReader extends BaseReader {
    static get basename () {
      return 'www.theguardian.com'
    }

    static get source () {
      return 'The Guardian'
    }

    get author () {
      let authors = []
      let that = this
      this.$('span[itemprop=name]').each(function () {
        authors.push(that.$(this).text())
      })
      return authors.join(', ')
    }

    get content () {
      let html = ''
      let that = this
      this.$('.content__article-body > p').each(function () {
        html += that.$('<div>').append(that.$(this).clone()).html()
      })
      return html
    }

    get datetime () {
      return this.$('time').attr('datetime')
    }

    get title () {
      return this.$('.content__headline').first().text()
    }
  }
  return TheGuardianReader
}
